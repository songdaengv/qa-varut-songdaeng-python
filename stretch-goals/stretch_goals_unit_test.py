import unittest
from .calculator_stretch_goal import calculator
from .factors_stretch_goal import factors
from .vowel_swapper_stretch_goal import vowel_swapper
class TestStringMethods(unittest.TestCase):

    def test_calculator(self):
        self.assertEqual(calculator(2, 4, "+"), '110')
        self.assertEqual(calculator(10, 3, "-"), '111')
        self.assertEqual(calculator(4, 7, "*"), '11100')
        self.assertEqual(calculator(100, 2, "/"), '110010')


    def test_factors(self):
        self.assertEqual(factors(15), [3, 5])
        self.assertEqual(factors(12), [2, 3, 4, 6])
        self.assertEqual(factors(13), '13 is a prime number')

    def test_vowel_swapper(self):
        self.assertEqual(vowel_swapper("aAa eEe iIi oOo uUu"), "a4a e3e i!i oOOOo u|_|u")
        self.assertEqual(vowel_swapper("Hello World"), "Hello Wooorld" )
        self.assertEqual(vowel_swapper("Everything's Available"), "Ev3rything's Av4!lable")


if __name__ == '__main__':
    unittest.main()