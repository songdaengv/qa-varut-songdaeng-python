def vowel_swapper(string):
    # ==============
    # Your code here
    dict = {'a': '4',
            'e': '3',
            'i': '!',
            'o': 'ooo',
            'u': '|_|'
            }
    dictCounter = {'a': 0,
            'e': 0,
            'i': 0,
            'o': 0,
            'u': 0
            }
    ls = list(string)
    #loop -> match key -> check if second instance
    for index in range(len(ls)):
        for key, value in dict.items():
            counter = dictCounter[key]
            if ls[index] == key:
                dictCounter[key] = counter + 1
                if dictCounter[key] == 2:
                    ls[index] = value
                    dictCounter[key] = 0
                    break
            elif ls[index] == key.upper():
                dictCounter[key] = counter + 1
                if dictCounter[key] == 2:
                    ls[index] = value.upper()
                    dictCounter[key] = 0
                    break

    return ''.join(ls)
    # ==============


print(vowel_swapper("aAa eEe iIi oOo uUu")) # Should print "a4a e3e i!i o000o u|_|u" to the console
print(vowel_swapper("Hello World")) # Should print "Hello Wooorld" to the console
print(vowel_swapper("Everything's Available")) # Should print "Ev3rything's Av4!lable" to the console
