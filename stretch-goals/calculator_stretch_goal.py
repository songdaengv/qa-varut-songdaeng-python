def calculator(a, b, operator):
    # ==============
    # Your code here
    total = 0
    try:
        if operator == '+':
            total = a + b
        elif operator == '-':
            total = a - b
        elif operator == '*':
            total = a * b
        elif operator == '/':
            total = a / b
        else:
            print("Incorrect Operator")
    except:
        print("An exception occurred")

    return "{0:b}".format(int(total))
    # ==============


print(calculator(2, 4, "+")) # Should print 110 to the console
print(calculator(10, 3, "-")) # Should print 111 to the console
print(calculator(4, 7, "*")) # Should output 11100 to the console
print(calculator(100, 2, "/")) # Should print 110010 to the console
