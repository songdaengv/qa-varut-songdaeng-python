def factors(number):
    # ==============
    # Your code here
    factor = []
    for index in range(2, number):
        if number % index == 0:
            factor.append(index)

    return factor if factor else str(number) + " is a prime number"
    # ==============


print(factors(15)) # Should print [3, 5] to the console
print(factors(12)) # Should print [2, 3, 4, 6] to the console
print(factors(13)) # Should print “13 is a prime number”
