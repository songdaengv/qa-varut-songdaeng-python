import unittest
from tasks.vowel_swapper import vowel_swapper
from tasks.calculator import calculator
from tasks.factors import  factors
class TestStringMethods(unittest.TestCase):

    def test_vowel_swapper(self):
        self.assertEqual(vowel_swapper("aA eE iI oO uU"), '44 33 !! oooOOO |_||_|')
        self.assertEqual(vowel_swapper("Hello World"), 'H3llooo Wooorld')
        self.assertEqual(vowel_swapper("Everything's Available"), "3v3ryth!ng's 4v4!l4bl3")

    def test_calculator(self):
        self.assertEqual(calculator(2, 4, "+"), 6)
        self.assertEqual(calculator(10, 3, "-"), 7)
        self.assertEqual(calculator(4, 7, "*"), 28)
        self.assertEqual(calculator(100, 2, "/"), 50)

    def test_factors(self):
        self.assertEqual(factors(15), [3, 5])
        self.assertEqual(factors(12), [2, 3, 4, 6])
        self.assertEqual(factors(13), [])


if __name__ == '__main__':
    unittest.main()