def vowel_swapper(string):
    # ==============
    # Your code here
    ###Create a function that accepts a string as an argument and swaps vowels with a corresponding symbol, such that:
    #- `a` becomes `4`
    #- `e` becomes `3`
    #- `i` becomes `!`
   # - `o` becomes `ooo`
    #- `u` becomes `|_|`

    dict = {'a': '4',
            'e': '3',
            'i': '!',
            'o': 'ooo',
            'u': '|_|'
            }
    ls = list(string)
    for index in range(len(ls)):
        for key, value in dict.items():
            if ls[index] == key:
                ls[index] = value
                break
            elif ls[index] == key.upper():
                ls[index] = value.upper()
                break

    return ''.join(ls)
    # ==============

print(vowel_swapper("aA eE iI oO uU")) # Should print "44 33 !! ooo000 |_||_|" to the console
print(vowel_swapper("Hello World")) # Should print "H3llooo Wooorld" to the console 
print(vowel_swapper("Everything's Available")) # Should print "3v3ryth!ng's 4v4!l4bl3" to the console