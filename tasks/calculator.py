def calculator(a, b, operator):
    # ==============
    # Your code here
    total = 0
    #cases to match the operator
    try:
        if operator == '+':
            total = a + b
        elif operator == '-':
            total = a - b
        elif operator == '*':
            total = a * b
        elif operator == '/':
            total = a / b
        else:
            print("Incorrect Operator")
    except:
        print("An exception occurred")

    return int(total)
    # ==============


print(calculator(2, 4, "+")) # Should print 6 to the console
print(calculator(10, 3, "-")) # Should print 7 to the console
print(calculator(4, 7, "*")) # Should print 28 to the console
print(calculator(100, 2, "/")) # Should print 50 to the console
